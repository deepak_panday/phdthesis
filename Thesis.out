\BOOKMARK [0][-]{Doc-Start}{Abstract}{}% 1
\BOOKMARK [0][-]{Doc-Start}{Acknowledgement}{}% 2
\BOOKMARK [0][-]{Doc-Start}{Publications}{}% 3
\BOOKMARK [0][-]{chapter*.2}{List of Figures}{}% 4
\BOOKMARK [0][-]{chapter*.3}{List of Tables}{}% 5
\BOOKMARK [0][-]{chapter.1}{Introdution}{}% 6
\BOOKMARK [1][-]{section.1.1}{Introduction and Objective}{chapter.1}% 7
\BOOKMARK [1][-]{section.1.2}{Contribution}{chapter.1}% 8
\BOOKMARK [1][-]{section.1.3}{Thesis Structure}{chapter.1}% 9
\BOOKMARK [0][-]{chapter.2}{Literature Review}{}% 10
\BOOKMARK [1][-]{section.2.1}{Topology of Clustering Algorithm}{chapter.2}% 11
\BOOKMARK [1][-]{section.2.2}{Clustering Algorithms}{chapter.2}% 12
\BOOKMARK [2][-]{subsection.2.2.1}{ k-Means}{section.2.2}% 13
\BOOKMARK [2][-]{subsection.2.2.2}{Initialization of centroids}{section.2.2}% 14
\BOOKMARK [2][-]{subsection.2.2.3}{Finding K in k-Means}{section.2.2}% 15
\BOOKMARK [1][-]{section.2.3}{Dimension Reduction}{chapter.2}% 16
\BOOKMARK [2][-]{subsection.2.3.1}{Feature Selection}{section.2.3}% 17
\BOOKMARK [1][-]{section.2.4}{Feature Weighting}{chapter.2}% 18
\BOOKMARK [1][-]{section.2.5}{Missing Values}{chapter.2}% 19
\BOOKMARK [0][-]{chapter.3}{Methodology}{}% 20
\BOOKMARK [1][-]{section.3.1}{Similarity Measurement}{chapter.3}% 21
\BOOKMARK [1][-]{section.3.2}{Data Standardization}{chapter.3}% 22
\BOOKMARK [1][-]{section.3.3}{Feature Weighting}{chapter.3}% 23
\BOOKMARK [1][-]{section.3.4}{k-Means algorithms and its weighting variant}{chapter.3}% 24
\BOOKMARK [2][-]{subsection.3.4.1}{Generic k-Means}{section.3.4}% 25
\BOOKMARK [2][-]{subsection.3.4.2}{ik-Means}{section.3.4}% 26
\BOOKMARK [2][-]{subsection.3.4.3}{Wk-Means}{section.3.4}% 27
\BOOKMARK [2][-]{subsection.3.4.4}{MWk-Means}{section.3.4}% 28
\BOOKMARK [1][-]{section.3.5}{Cluster Validity}{chapter.3}% 29
\BOOKMARK [1][-]{section.3.6}{Feature Selection}{chapter.3}% 30
\BOOKMARK [2][-]{subsection.3.6.1}{intelligent K-Means Feature Selection\(ikFS\)}{section.3.6}% 31
\BOOKMARK [2][-]{subsection.3.6.2}{Feature selection using feature similarity}{section.3.6}% 32
\BOOKMARK [2][-]{subsection.3.6.3}{Multi-Cluster Feature Selection}{section.3.6}% 33
\BOOKMARK [2][-]{subsection.3.6.4}{Feature selection via Feature Weighting}{section.3.6}% 34
\BOOKMARK [1][-]{section.3.7}{Missing Data}{chapter.3}% 35
\BOOKMARK [2][-]{subsection.3.7.1}{Missing Data Patterns}{section.3.7}% 36
\BOOKMARK [2][-]{subsection.3.7.2}{Missing-data Mechanism}{section.3.7}% 37
\BOOKMARK [2][-]{subsection.3.7.3}{Dealing with Missing Attribute Values}{section.3.7}% 38
\BOOKMARK [1][-]{section.3.8}{Dataset}{chapter.3}% 39
\BOOKMARK [2][-]{subsection.3.8.1}{Synthetic Gaussian Distribution}{section.3.8}% 40
\BOOKMARK [2][-]{subsection.3.8.2}{Real Data}{section.3.8}% 41
\BOOKMARK [2][-]{subsection.3.8.3}{Noise}{section.3.8}% 42
\BOOKMARK [2][-]{subsection.3.8.4}{Noise Strength}{section.3.8}% 43
\BOOKMARK [2][-]{subsection.3.8.5}{Noise Distribution}{section.3.8}% 44
\BOOKMARK [2][-]{subsection.3.8.6}{Noise Model}{section.3.8}% 45
\BOOKMARK [1][-]{section.3.9}{Introduction}{chapter.3}% 46
\BOOKMARK [1][-]{section.3.10}{Aim}{chapter.3}% 47
\BOOKMARK [1][-]{section.3.11}{Transform of dataset to different p-norms }{chapter.3}% 48
\BOOKMARK [2][-]{subsection.3.11.1}{Transformation of a dataset from p1-norm to p2-norm}{section.3.11}% 49
\BOOKMARK [2][-]{subsection.3.11.2}{Validation of transformation process}{section.3.11}% 50
\BOOKMARK [2][-]{subsection.3.11.3}{Generation of a Gaussian mixed model in different p-norms}{section.3.11}% 51
\BOOKMARK [1][-]{section.3.12}{Effect of p-norms over clustering algorithms}{chapter.3}% 52
\BOOKMARK [2][-]{subsection.3.12.1}{Performance of different aggregation functions}{section.3.12}% 53
\BOOKMARK [2][-]{subsection.3.12.2}{Compare different aggregation functions}{section.3.12}% 54
\BOOKMARK [2][-]{subsection.3.12.3}{p-norms vs distance coefficient}{section.3.12}% 55
\BOOKMARK [1][-]{section.3.13}{Conclusion}{chapter.3}% 56
\BOOKMARK [0][-]{chapter.4}{Prediction of Missing Values}{}% 57
\BOOKMARK [1][-]{subsection.4.0.1}{Aim}{chapter.4}% 58
\BOOKMARK [1][-]{section.4.1}{SETTING UP THE EXPERIMENT}{chapter.4}% 59
\BOOKMARK [2][-]{subsection.4.1.1}{Setting up dataset}{section.4.1}% 60
\BOOKMARK [2][-]{subsection.4.1.2}{Addition of Missing Values}{section.4.1}% 61
\BOOKMARK [2][-]{subsection.4.1.3}{Selection of Algorithms}{section.4.1}% 62
\BOOKMARK [2][-]{subsection.4.1.4}{Extension to Noise}{section.4.1}% 63
\BOOKMARK [2][-]{subsection.4.1.5}{Listing of All Experiments}{section.4.1}% 64
\BOOKMARK [1][-]{section.4.2}{Reading Results}{chapter.4}% 65
\BOOKMARK [2][-]{subsection.4.2.1}{Organization of Results}{section.4.2}% 66
\BOOKMARK [2][-]{subsection.4.2.2}{Reading Header from tables}{section.4.2}% 67
\BOOKMARK [1][-]{section.4.3}{Experimental Results}{chapter.4}% 68
\BOOKMARK [2][-]{subsection.4.3.1}{Imputation of Different Algorithms for Weighted k-Means and Intelligent Weighted k-Means}{section.4.3}% 69
\BOOKMARK [2][-]{subsection.4.3.2}{Evaluation of Partial Distance against Different Methods in k-Means and Weighted k-Means }{section.4.3}% 70
\BOOKMARK [0][-]{chapter.5}{Dimension Reduction}{}% 71
\BOOKMARK [1][-]{section.5.1}{Aims}{chapter.5}% 72
\BOOKMARK [1][-]{section.5.2}{Experimental Set up}{chapter.5}% 73
\BOOKMARK [2][-]{subsection.5.2.1}{Dataset Setting up}{section.5.2}% 74
\BOOKMARK [2][-]{subsection.5.2.2}{Parameter Tuning}{section.5.2}% 75
\BOOKMARK [1][-]{section.5.3}{Result Presentation}{chapter.5}% 76
\BOOKMARK [2][-]{subsection.5.3.1}{Synthetic Dataset}{section.5.3}% 77
\BOOKMARK [2][-]{subsection.5.3.2}{UCI datasets}{section.5.3}% 78
\BOOKMARK [1][-]{section.5.4}{Conclusion}{chapter.5}% 79
\BOOKMARK [0][-]{chapter.6}{Extended Experiments}{}% 80
\BOOKMARK [1][-]{section.6.1}{Experimental set-up}{chapter.6}% 81
\BOOKMARK [1][-]{section.6.2}{Extension of feature selection}{chapter.6}% 82
\BOOKMARK [2][-]{subsection.6.2.1}{Percentage of noisy feature selection}{section.6.2}% 83
\BOOKMARK [2][-]{subsection.6.2.2}{Extending the method to larger datasets}{section.6.2}% 84
\BOOKMARK [2][-]{subsection.6.2.3}{Performance of classifiers after feature selection}{section.6.2}% 85
\BOOKMARK [2][-]{subsection.6.2.4}{Performance of feature selection in Lp space}{section.6.2}% 86
\BOOKMARK [1][-]{section.6.3}{Extension of partial distance to the Minkowski metric }{chapter.6}% 87
\BOOKMARK [1][-]{section.6.4}{Conclusion}{chapter.6}% 88
\BOOKMARK [0][-]{chapter.7}{Discussion and Conclusion }{}% 89
\BOOKMARK [1][-]{section.7.1}{Research outcomes}{chapter.7}% 90
\BOOKMARK [1][-]{section.7.2}{Observations}{chapter.7}% 91
\BOOKMARK [1][-]{section.7.3}{Limitations}{chapter.7}% 92
\BOOKMARK [1][-]{section.7.4}{Further research}{chapter.7}% 93
\BOOKMARK [0][-]{Appendix.a.A}{Appendix Coordinates System}{}% 94
\BOOKMARK [1][-]{section.a.A.1}{Cartesian to polar conversion}{Appendix.a.A}% 95
\BOOKMARK [0][-]{Appendix.a.B}{Appendix Gaussian Distribution}{}% 96
\BOOKMARK [1][-]{subsection.a.B.0.1}{Standard Gaussian Distribution}{Appendix.a.B}% 97
\BOOKMARK [2][-]{subsection.a.B.0.2}{Gaussian Distribution in two Dimension}{subsection.a.B.0.1}% 98
\BOOKMARK [2][-]{subsection.a.B.0.3}{Generalization of Gaussian Distribution}{subsection.a.B.0.1}% 99
\BOOKMARK [0][-]{Appendix.a.C}{Appendix Gaussian Mixed Model}{}% 100
\BOOKMARK [1][-]{subsection.a.C.0.1}{Gaussian Mixed Model in one Dimension}{Appendix.a.C}% 101
\BOOKMARK [2][-]{subsection.a.C.0.2}{Gaussian Mixed Model \(GMM\) for two Dimensions}{subsection.a.C.0.1}% 102
\BOOKMARK [0][-]{Appendix.a.D}{Appendix GMM Control Parameters and k}{}% 103
\BOOKMARK [1][-]{section.a.D.1}{Control Parameters in GMM}{Appendix.a.D}% 104
\BOOKMARK [2][-]{subsection.a.D.1.1}{Visualization of GMM Datasets}{section.a.D.1}% 105
\BOOKMARK [2][-]{subsection.a.D.1.2}{Observation based on cluster recovery}{section.a.D.1}% 106
\BOOKMARK [0][-]{Appendix.a.E}{Appendix Effect of Noise in k-means}{}% 107
