\chapter{Extended Experiments}\label{ch:ExtendedExperimetns}
In the previous three chapters, results from the implementation of feature weighting principles in three areas of clustering - p-norm datasets, datasets with missing values and feature selection are discussed in detail. This chapter further extends our knowledge of cluster analysis and aims to answer the following questions:
\begin{enumerate}[(i)]


	\item Are the proposed feature selection algorithms effectively remove noisy features?
	\item Are the proposed feature selection algorithms suitable for larger datasets?
	\item Are the proposed feature selection algorithms able to retain the features, required by classifiers?
	\item Is the performance of  proposed feature selection algorithms better in p-norms?
	\item Can partial distance approaches can be implemented with the Minkowski metric in p-norms datasets?
	
\end{enumerate}

\section{Experimental set-up}

To answer the above questions, experiments are conducted on both the synthetic and real-world (UCI) datasets introduced earlier, in Chapter \ref{ch:Methology}. Research in this chapter is restricted to the added noisy features, where noisy features are generated from the Uniform distribution. Three cases of added noisy features - no noise, half noise and full noise - are analysed. The addition of noisy features in the tables below is denoted by the datasetName+\textit{K}NF, where K represents either 0 or $\frac{\|V\|}{2}$ or $\|V\|$ number of added noisy features with $\|V\|$ feature space cardinality. For example, 1000x8-2+4NF represents a dataset with two Gaussian clusters, 1000 data points and 8  original features. The dataset (1000x8-2) has 4 more noisy features added on it.\\

\noindent
In Chapter \ref{ch:DimensionReduction}, the intelligent \textit{k}-Means for feature selection, iKFS, is observed to have  no role in the selection of features, hence, iKFS is discarded. Therefore, in addition to our proposed feature selection methods, feature similarity based on feature selection, FSFS, and multi-cluster based feature selection, MCFS, are analysed further.\\

\noindent
As in the earlier experiments, the Silhouette index is used for parameter tuning in FSFS and MCFS (see \cite{panday2018feature} for further detail). In FSFS and MCFS, the best case is chosen by using the label information available (supervised learning). The first three questions set-up above - (i), (ii) and (iii) - are analysed by setting the distance coefficient (p) to 2.\\

\noindent
In our earlier chapter, the value of p lies between 1.1 to 5.0, with an interval of 0.1. However, to observe the trend of p, a larger interval of 0.5 can be considered \cite{}. Therefore, to answer questions (iv) and (v),  the values of $p_1$ for $p_1$-norm and the coefficient of the Minkowski metric, $p_2$, in \textit{mean}-FSFW and \textit{max}-FSFW are set to \{1.1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5\}. \\

\noindent
Experiments are run in both the synthetic and real-world (UCI) datasets  to answer questions(i) and (iii). Two datasets from the UCI repository which have higher feature space cardinality, i.e., $\|V\| \geq 100$,  are considered, for question (ii). To answer questions (iv) and (v), experiments are conducted in the synthetic datasets. \\%as no real-world datasets in p-norms other than 2-norm are available to our knowledge.\\




\section{Extension of feature selection}

In Chapter \ref{ch:DimensionReduction}, performance of the proposed feature selection algorithms is observed on the basis of cluster recovery and percentage of the feature selection. In \cite{panday2018feature}, we published the results of using the feature selection algorithm on the basis of the percentage of noisy feature selected and have extended the work in two larger datasets from the UCI repository.




\subsection{Percentage of noisy feature selection}


In the two tables \ref{tbl:FS_NoisePerc_Syn} and \ref{tbl:FS_NoisePerc_Real}, results from synthetic and the real-world (UCI) datasets are summarized, showing the percentage of noisy features selected.
\subsubsection{Synthetic datasets}
\input{table/CH8_ExtendedWork/FS_NoisePercentage.tex}

\noindent
\textit{mean}-FSFW is able to remove almost all added noisy features from the Gaussian clusters. \textit{max}-FSFW is able to remove most of the noisy features for larger datasets and up to 92.5\% of noisy features in the Gaussian clusters with feature space cardinality of 8+4NF. FSFS and MCFS are hardly able to remove any noisy features as the maximum percentage of noisy features removed is around 70\% .

\FloatBarrier

\subsubsection{Real-world (UCI) datasets}

\input{table/CH8_ExtendedWork/FS_NoisePercentageReal.tex}

\textit{mean}-FSFW is able to remove all noisy features from eight out of ten UCI datasets. In the Car Evaluation and the Tic-Tac-Toe datasets, \textit{mean}-FSFW is not able to remove even a single noisy features. In comparison, \textit{max}-FSFW is more effective than \textit{mean}-FSFW,  as  \textit{max}-FSFW  is able to remove almost all noisy features from ten UCI datasets except the Car Evaluation dataset with full noise. \\

\noindent
Under supervised learning, i.e. ``Best Case", MCFS is also effective for removing noisy features. However, performance is significantly reduced when the Silhouette index is used for parameter tuning, i.e. unsupervised learning.  FSFS is least effective in removing the noisy features in both supervised and unsupervised learning. FSFS with its Best Case (supervised learning) is far better at removing noisy features than the unsupervised learning method.


 
\FloatBarrier

%\input{table/CH8_ExtendedWork/NoisyFeatureSel.tex}
\subsection{Extending the method to larger datasets}

To observe the performance of our proposed feature selection algorithms for larger datasets, the ``Low Resolution Spectrometer" and ``Glass Sensor Array Batch10" datasets are considered. The ``Low Resolution Spectrometer" dataset has 101 features, whereas the ``Gas Sensor Array Batch10" dataset has 129 features. In the ``Low Resolution Spectrometer" dataset, 51 and 65 noisy features are added to create half and full noise datasets, whereas, in ``Gas-Sensor-Array-Batch10" dataset, 65 and 129 noisy features are added. All noisy features are drawn from the Uniform distribution. The performance of the algorithms are observed on the basis of cluster recovery (ARI) and the percentage of noisy feature selected.\\


\input{table/CH8_ExtendedWork/LargeDataSet.tex}
\FloatBarrier

\begin{itemize}

\item Cluster recovery (ARI)

\textit{mean}-FSFW and \textit{max}-FSFW are able to improve the cluster recovery for both datasets in most cases, with and without added noise, since the  ARI index is better in all cases compared to \textit{k}-Means (without feature selection). \textit{mean}-FSFW is the best for the ``Low Resolution Spectrometer" dataset with higher noise and the ``Gas Sensor Array Batch10" dataset when there are no noisy features. In most cases, \textit{mean}-FSFW and \textit{max}-FSFW are able to match the cluster recovery index (ARI). In some cases, \textit{mean}-FSFW and \textit{max}-FSFW  are even better than the ``Best Case" of FSFS and MCFS.

\item Percentage of noisy feature selected

\textit{mean}-FSFW is able to remove all added noisy features in both datasets. \textit{max}-FSFW also gives the same results, except with the ``Low Resolution Spectrometer" dataset with full noise. MCFS is also able to remove all added noisy features from the ``Gas Sensor Array Drift Batch10" dataset in supervised and unsupervised learning. When compared with FSFS, it is better at removing added noisy features in the ``Low Resolution Spectrometer" dataset, as FSFS has no contribution to make in removing the added noisy features.



\end{itemize}
\subsection{Performance of classifiers after feature selection}

Feature selection algorithms are normally considered as a first step in data mining and are used before classification. Feature selection algorithms are expected to retain the ``important" features required for classification. Therefore, the effect of proposed feature selection algorithms in three classifiers - KNN, Decision tree and Navie bayes - are observed with 10-fold cross-validation on  both synthetic and the real-world (UCI) datasets. The performance of these classifiers is measured using the ARI score before and after the feature selection. In KNN, the value of K is set to the square root of the number of entities in the dataset.\\

\noindent
Table \ref{tb:CombinedResult} and Table \ref{tb:CombinedResult-REAL} show the average ARI score by the classifiers  in synthetic and the real-world datasets with and without feature selection. 
\FloatBarrier


\subsubsection{Synthetic datasets}

\input{table/CH8_ExtendedWork/FS_GMM_Classifier.tex}
\FloatBarrier





\subsubsection{Real-world (UCI) datasets}

\input{table/CH8_ExtendedWork/FS_Real_Classifier.tex}
\FloatBarrier

The experiments clearly show that the feature selection can still retain the true or important features required by the classification algorithms, in most of the datasets.

\subsection{Performance of feature selection in $L_p$ space}

One of the areas of our research is analysis the performance of the cluster recovery algorithms in p-norms. In the table below, the effects of four feature selection algorithms on cluster recovery  are observed in Gaussian clusters defined in different p-norms. The synthetic datasets considered earlier are translated into different $p_1$-norm, where $p_1 \in \{ 1.1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5 \}$.\\

\noindent
The Silhouette index is used to identify best parameters in FSFS and MCFS. In the previously described cases,  the distance coefficient ($p_2$) is set to 2 in \textit{mean}-FSFW and \textit{max}-FSFW. For this experiment, the value of $p_2 \in \{ 1.1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5 \}$. The Silhouette index is used to find the best value of the distance coefficient, $p_2$, in  \textit{mean}-FSFW and \textit{max}-FSFW  for a dataset defined in $p_1$-norm.

 
\input{table/CH8_ExtendedWork/FS_extendedToLP.tex}

\noindent
The table above clearly shows that both \textit{mean}-FSFW and \textit{max}-FSFW outperform FSFS and MCFS in the Gaussian mixed-model, defined in different $p_1$-norms using unsupervised learning with or without added noisy features. Moreover, \textit{max}-FSFW is better than \textit{mean}-FSFW for all $p_1$-norms when there are no noisy features added in the Gaussian clusters. With the addition of the noisy features, \textit{mean}-FSFW is slightly better than \textit{max}-FSFW.




\FloatBarrier

\section{Extension of partial distance to the Minkowski metric }

The partial distance approach used in Chapter \ref{ch:MissingValues}, as an alternative to imputation methods, shows  promising results. The partial distance approach is further extended to the Minkowski metric in a way that the proposed feature selection algorithms can address the missing values implicitly. The extended algorithms are called  \textit{mean}FSFWext\textit{PD} and \textit{max}FSFWext\textit{PD}.

For the experimental set-up, the missing values are introduced in the first two features of the synthetic datasets used in earlier experiments. The missing values are missing completely at random, MCAR, and the percentage of the missing values is set to 40\%. The performance of the extended algorithms is compared with three imputation methods used in earlier chapter (Chapter \ref{ch:MissingValues}) and are further experimented in Gaussian clusters, defined in different p-norms. The performance is measured based on the adjusted Rand Index.

\input{table/CH8_ExtendedWork/FSFSextPDnew.tex}


\begin{itemize}
\item p-norm, $p<2$

Average imputation is found to be the best options for addressing missing values in \textit{mean}-FSFW and \textit{max}-FSFW for the Gaussian clusters, defined with p-norms, where $p<2$.
\item p-norm, $p=2$

When the Gaussian clusters are defined in 2-norm, the partial distance approach is found to be the best in both  proposed feature selection algorithms.
\item p-norm, $p>2$

For the Gaussian clusters, defined in p-norms, where $p > 2$, an average imputation method is the best option for replacing missing values, closely followed by the partial distance approaches for the proposed feature selection algorithms. 
\end{itemize}


\section{Conclusion}

In this chapter, extended experiments are conducted in the three different areas of cluster analysis carried out in previous chapters, and are combined together. It has been shown that the proposed feature selection algorithms are able to remove noisy features from both synthetic and the real-world datasets more effectively. The proposed feature selection algorithms are also suitable for the selection of the ``informative" features  from larger datasets and are  able to retain the ``true" features, required by classifiers. \\

\noindent
Moreover, the proposed feature selection algorithms provide the best alternative for feature selection of Gaussian clusters, defined in different p-norms. The partial distance approach is found to be the best alternative to imputation methods for addressing missing values in Gaussian clusters, defined in 2-norm.
	
	