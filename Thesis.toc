\contentsline {chapter}{Abstract}{i}{Doc-Start}
\contentsline {chapter}{Acknowledgement}{i}{Doc-Start}
\contentsline {chapter}{Publications}{i}{Doc-Start}
\contentsline {chapter}{List of Figures}{v}{chapter*.2}
\contentsline {chapter}{List of Tables}{viii}{chapter*.3}
\contentsline {chapter}{\numberline {1}Introdution}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction and Objective}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Contribution}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Thesis Structure}{2}{section.1.3}
\contentsline {chapter}{\numberline {2}Literature Review}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Topology of Clustering Algorithm}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Clustering Algorithms}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1} \textit {k}-Means}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Initialization of centroids}{5}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Finding K in \textit {k}-Means}{6}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Dimension Reduction}{7}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Feature Selection}{7}{subsection.2.3.1}
\contentsline {subsubsection}{Some Common Practice in Feature Selection}{8}{section*.6}
\contentsline {subsubsection}{Semi-Supervised Feature Selection }{9}{section*.7}
\contentsline {section}{\numberline {2.4}Feature Weighting}{9}{section.2.4}
\contentsline {section}{\numberline {2.5}Missing Values}{11}{section.2.5}
\contentsline {chapter}{\numberline {3}Methodology}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Similarity Measurement}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Data Standardization}{14}{section.3.2}
\contentsline {section}{\numberline {3.3}Feature Weighting}{15}{section.3.3}
\contentsline {section}{\numberline {3.4}\textit {k}-Means algorithms and its weighting variant}{16}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Generic \textit {k}-Means}{16}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}\textit {ik}-Means}{17}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}\textit {Wk}-Means}{17}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}\textit {MWk}-Means}{18}{subsection.3.4.4}
\contentsline {section}{\numberline {3.5}Cluster Validity}{18}{section.3.5}
\contentsline {section}{\numberline {3.6}Feature Selection}{19}{section.3.6}
\contentsline {subsection}{\numberline {3.6.1}intelligent K-Means Feature Selection(ikFS)}{19}{subsection.3.6.1}
\contentsline {subsection}{\numberline {3.6.2}Feature selection using feature similarity}{19}{subsection.3.6.2}
\contentsline {subsection}{\numberline {3.6.3}Multi-Cluster Feature Selection}{20}{subsection.3.6.3}
\contentsline {subsection}{\numberline {3.6.4}Feature selection via Feature Weighting}{21}{subsection.3.6.4}
\contentsline {section}{\numberline {3.7}Missing Data}{21}{section.3.7}
\contentsline {subsection}{\numberline {3.7.1}Missing Data Patterns}{21}{subsection.3.7.1}
\contentsline {subsection}{\numberline {3.7.2}Missing-data Mechanism}{23}{subsection.3.7.2}
\contentsline {subsection}{\numberline {3.7.3}Dealing with Missing Attribute Values}{23}{subsection.3.7.3}
\contentsline {subsubsection}{Imputation Methods}{23}{section*.12}
\contentsline {subsubsection}{Attribute mean value}{23}{section*.13}
\contentsline {subsubsection}{\textit {k}-Nearest Neighbour Techniques}{24}{section*.14}
\contentsline {subsubsection}{Regression Based Imputation}{25}{section*.17}
\contentsline {subsubsection}{No Imputation}{26}{section*.18}
\contentsline {subsubsection}{Partial Distance}{26}{section*.19}
\contentsline {section}{\numberline {3.8}Dataset}{27}{section.3.8}
\contentsline {subsection}{\numberline {3.8.1}Synthetic Gaussian Distribution}{27}{subsection.3.8.1}
\contentsline {subsection}{\numberline {3.8.2}Real Data}{28}{subsection.3.8.2}
\contentsline {subsection}{\numberline {3.8.3}Noise}{28}{subsection.3.8.3}
\contentsline {subsection}{\numberline {3.8.4}Noise Strength}{28}{subsection.3.8.4}
\contentsline {subsection}{\numberline {3.8.5}Noise Distribution}{29}{subsection.3.8.5}
\contentsline {subsection}{\numberline {3.8.6}Noise Model}{29}{subsection.3.8.6}
\contentsline {section}{\numberline {3.9}Introduction}{30}{section.3.9}
\contentsline {section}{\numberline {3.10}Aim}{30}{section.3.10}
\contentsline {section}{\numberline {3.11}Transform of dataset to different p-norms }{30}{section.3.11}
\contentsline {subsection}{\numberline {3.11.1}Transformation of a dataset from $p_1$-norm to $p_2$-norm}{31}{subsection.3.11.1}
\contentsline {subsection}{\numberline {3.11.2}Validation of transformation process}{32}{subsection.3.11.2}
\contentsline {subsubsection}{Visualization of transformation process in 2D}{32}{section*.23}
\contentsline {subsection}{\numberline {3.11.3}Generation of a Gaussian mixed model in different p-norms}{35}{subsection.3.11.3}
\contentsline {section}{\numberline {3.12}Effect of p-norms over clustering algorithms}{36}{section.3.12}
\contentsline {subsection}{\numberline {3.12.1}Performance of different aggregation functions}{37}{subsection.3.12.1}
\contentsline {subsubsection}{ Average cluster recovery}{38}{section*.29}
\contentsline {subsubsection}{ $p_2$ equals to $p_1$}{38}{section*.31}
\contentsline {subsubsection}{$p_2$ equals to 2}{39}{section*.33}
\contentsline {subsubsection}{$p_2$ that yield max ARI}{40}{section*.35}
\contentsline {subsection}{\numberline {3.12.2}Compare different aggregation functions}{41}{subsection.3.12.2}
\contentsline {subsection}{\numberline {3.12.3}p-norms vs distance coefficient}{43}{subsection.3.12.3}
\contentsline {subsubsection}{$p_1$ vs $p_2$ in \textit {MWk}-Means}{44}{section*.39}
\contentsline {subsubsection}{p1 vs p2 in \textit {iMWk}-Means}{44}{section*.41}
\contentsline {subsubsection}{Additional Experiment..... finding best P2 using different index}{45}{section*.43}
\contentsline {section}{\numberline {3.13}Conclusion}{45}{section.3.13}
\contentsline {chapter}{\numberline {4}Prediction of Missing Values}{46}{chapter.4}
\contentsline {subsection}{\numberline {4.0.1}Aim}{46}{subsection.4.0.1}
\contentsline {section}{\numberline {4.1}SETTING UP THE EXPERIMENT}{47}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Setting up dataset}{47}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Addition of Missing Values}{47}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Selection of Algorithms}{48}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Extension to Noise}{48}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Listing of All Experiments}{48}{subsection.4.1.5}
\contentsline {section}{\numberline {4.2}Reading Results}{49}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Organization of Results}{49}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Reading Header from tables}{50}{subsection.4.2.2}
\contentsline {subsubsection}{Results for Imputation Methods}{50}{section*.45}
\contentsline {subsubsection}{Results from Different Approaches}{50}{section*.46}
\contentsline {section}{\numberline {4.3}Experimental Results}{50}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Imputation of Different Algorithms for Weighted \textit {k}-Means and Intelligent Weighted \textit {k}-Means}{50}{subsection.4.3.1}
\contentsline {subsubsection}{Missing values with MCAR Mechanism}{50}{section*.47}
\contentsline {subsubsection}{Missing values with NMAR Mechanism}{56}{section*.52}
\contentsline {subsection}{\numberline {4.3.2}Evaluation of Partial Distance against Different Methods in \textit {k}-Means and Weighted \textit {k}-Means }{61}{subsection.4.3.2}
\contentsline {subsubsection}{Missing values with MCAR Mechanism}{61}{section*.57}
\contentsline {subsubsection}{Missing values with NMAR Mechanism}{67}{section*.62}
\contentsline {subsubsection}{Conclusion}{72}{section*.67}
\contentsline {chapter}{\numberline {5}Dimension Reduction}{74}{chapter.5}
\contentsline {section}{\numberline {5.1}Aims}{74}{section.5.1}
\contentsline {section}{\numberline {5.2}Experimental Set up}{75}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Dataset Setting up}{75}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Parameter Tuning}{75}{subsection.5.2.2}
\contentsline {section}{\numberline {5.3}Result Presentation}{76}{section.5.3}
\contentsline {subsubsection}{Organization of Results}{76}{section*.68}
\contentsline {subsubsection}{Results from Synthetic datasets}{76}{section*.69}
\contentsline {subsubsection}{Organizing from real-world datasets}{76}{section*.70}
\contentsline {subsubsection}{Table Organization}{77}{section*.71}
\contentsline {subsection}{\numberline {5.3.1}Synthetic Dataset}{77}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}UCI datasets}{83}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Conclusion}{88}{section.5.4}
\contentsline {chapter}{\numberline {6}Extended Experiments}{89}{chapter.6}
\contentsline {section}{\numberline {6.1}Experimental set-up}{89}{section.6.1}
\contentsline {section}{\numberline {6.2}Extension of feature selection}{90}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Percentage of noisy feature selection}{90}{subsection.6.2.1}
\contentsline {subsubsection}{Synthetic datasets}{90}{section*.78}
\contentsline {subsubsection}{Real-world (UCI) datasets}{90}{section*.80}
\contentsline {subsection}{\numberline {6.2.2}Extending the method to larger datasets}{91}{subsection.6.2.2}
\contentsline {subsection}{\numberline {6.2.3}Performance of classifiers after feature selection}{92}{subsection.6.2.3}
\contentsline {subsubsection}{Synthetic datasets}{92}{section*.83}
\contentsline {subsubsection}{Real-world (UCI) datasets}{92}{section*.85}
\contentsline {subsection}{\numberline {6.2.4}Performance of feature selection in $L_p$ space}{93}{subsection.6.2.4}
\contentsline {section}{\numberline {6.3}Extension of partial distance to the Minkowski metric }{94}{section.6.3}
\contentsline {section}{\numberline {6.4}Conclusion}{94}{section.6.4}
\contentsline {chapter}{\numberline {7}Discussion and Conclusion }{95}{chapter.7}
\contentsline {section}{\numberline {7.1}Research outcomes}{95}{section.7.1}
\contentsline {section}{\numberline {7.2}Observations}{95}{section.7.2}
\contentsline {section}{\numberline {7.3}Limitations}{96}{section.7.3}
\contentsline {section}{\numberline {7.4}Further research}{96}{section.7.4}
\contentsline {chapter}{Appendix \numberline {A}Coordinates System}{98}{Appendix.a.A}
\contentsline {section}{\numberline {A.1}Cartesian to polar conversion}{98}{section.a.A.1}
\contentsline {chapter}{Appendix \numberline {B}Gaussian Distribution}{100}{Appendix.a.B}
\contentsline {subsection}{\numberline {B.0.1}Standard Gaussian Distribution}{100}{subsection.a.B.0.1}
\contentsline {subsection}{\numberline {B.0.2}Gaussian Distribution in two Dimension}{101}{subsection.a.B.0.2}
\contentsline {subsection}{\numberline {B.0.3}Generalization of Gaussian Distribution}{101}{subsection.a.B.0.3}
\contentsline {chapter}{Appendix \numberline {C}Gaussian Mixed Model}{103}{Appendix.a.C}
\contentsline {subsection}{\numberline {C.0.1}Gaussian Mixed Model in one Dimension}{103}{subsection.a.C.0.1}
\contentsline {subsection}{\numberline {C.0.2}Gaussian Mixed Model (GMM) for two Dimensions}{104}{subsection.a.C.0.2}
\contentsline {chapter}{Appendix \numberline {D}GMM Control Parameters and \textit {k}}{106}{Appendix.a.D}
\contentsline {section}{\numberline {D.1}Control Parameters in GMM}{106}{section.a.D.1}
\contentsline {subsection}{\numberline {D.1.1}Visualization of GMM Datasets}{106}{subsection.a.D.1.1}
\contentsline {subsection}{\numberline {D.1.2}Observation based on cluster recovery}{108}{subsection.a.D.1.2}
\contentsline {chapter}{Appendix \numberline {E}Effect of Noise in \textit {k}-means}{112}{Appendix.a.E}
